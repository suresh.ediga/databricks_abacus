terraform {
  required_providers {
    databricks = {
      source  = "databrickslabs/databricks"
      version = "0.5.2"

    }
    aws = {
      source  = "hashicorp/aws"
      version = "3.49.0"
    }
  }
}
variable "databricks_account_id" {}
variable "databricks_account_username" {}
variable "databricks_account_password" {}
variable "region" {}
variable "workspace_vpce_service" {}
variable "relay_vpce_service" {}
variable "vpce_subnet_cidr" {}
variable "private_dns_enabled" { default = false }
variable "tags" { default = {} }

locals {
  prefix = "private-link-ws"
}

provider "aws" {
  region = var.region
}

module "this" {
  source                      = "../../modules/aws-e2workspace"
  databricks_account_id       = var.databricks_account_id
  databricks_account_password = var.databricks_account_password
  databricks_account_username = var.databricks_account_username
  vpce_subnet_cidr            = var.vpce_subnet_cidr
  workspace_vpce_service      = var.workspace_vpce_service
  relay_vpce_service          = var.relay_vpce_service
  private_dns_enabled         = var.private_dns_enabled
  //cross_account_arn           = var.cross_account_arn

  region = "us-east-1"

  tags = {
    Owner = "suresh.ediga@abacusinsights.com"
  }
}

# output "databricks_host" {
#   value = module.this.databricks_host
# }