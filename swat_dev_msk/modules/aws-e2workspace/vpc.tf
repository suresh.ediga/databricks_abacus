
data "aws_availability_zones" "available" {}

module "vpc" {

  source  = "terraform-aws-modules/vpc/aws"
  version = "3.2.0"
 
  name = local.prefix
  cidr = var.cidr_block
  azs  = ["us-east-1b", "us-east-1a", "us-east-1c"] # make this a variable later because different for different envs

  tags = var.tags

  enable_dns_hostnames = true
  enable_nat_gateway   = false //for private link
  single_nat_gateway   = true
  create_igw           = false //for private link


  private_subnets = [cidrsubnet(var.cidr_block, 3,1), cidrsubnet(var.cidr_block, 3, 2),cidrsubnet(var.cidr_block, 3, 3)]

 
  manage_default_security_group = true
  default_security_group_name = "${local.prefix}-sg"


  default_security_group_egress = [{
    cidr_blocks = "0.0.0.0/0"
  }]

  default_security_group_ingress = [{
    description = "Allow all internal TCP and UDP"
    self        = true
  }]
}


module "vpc_endpoints" {
  source  = "terraform-aws-modules/vpc/aws//modules/vpc-endpoints"
  version = "3.2.0"

  vpc_id             = module.vpc.vpc_id
  security_group_ids = [module.vpc.default_security_group_id]

  endpoints = {
    s3 = {
      service      = "s3"
      service_type = "Gateway"

      route_table_ids = flatten([
      module.vpc.private_route_table_ids,
      ]
      )

      tags = {
        Name = "${local.prefix}-s3-vpc-endpoint"
      }
    },
    sts = {
      service             = "sts"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
      tags = {
        Name = "${local.prefix}-sts-vpc-endpoint"
      }
    }

  }

  tags = var.tags
}


data "aws_vpc" "prod" {
  id = module.vpc.vpc_id
}

// this subnet houses the data plane VPC endpoints
resource "aws_subnet" "dataplane_vpce" {
  vpc_id     = module.vpc.vpc_id
  cidr_block = var.vpce_subnet_cidr //required to change

  tags = merge(data.aws_vpc.prod.tags, {
    Name = "${local.prefix}-${data.aws_vpc.prod.id}-pl-vpce"
  })
}



resource "aws_route_table" "this" {
  vpc_id = module.vpc.vpc_id

  tags = merge(data.aws_vpc.prod.tags, {
    Name = "${local.prefix}-${data.aws_vpc.prod.id}-pl-local-route-tbl"
  })
}

resource "aws_route_table_association" "dataplane_vpce_rtb" {
  subnet_id      = aws_subnet.dataplane_vpce.id
  route_table_id = aws_route_table.this.id
}




resource "aws_security_group" "dataplane_vpce" {
  name        = "Data Plane VPC endpoint security group"
  description = "Security group shared with relay and workspace endpoints"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "Inbound rules"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = concat([var.vpce_subnet_cidr], module.vpc.private_subnets_cidr_blocks)
  }

  ingress {
    description = "Inbound rules"
    from_port   = 6666
    to_port     = 6666
    protocol    = "tcp"
    cidr_blocks = concat([var.vpce_subnet_cidr], module.vpc.private_subnets_cidr_blocks)
  }

  egress {
    description = "Outbound rules"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = concat([var.vpce_subnet_cidr],  module.vpc.private_subnets_cidr_blocks)
  }

  egress {
    description = "Outbound rules"
    from_port   = 6666
    to_port     = 6666
    protocol    = "tcp"
    cidr_blocks = concat([var.vpce_subnet_cidr],  module.vpc.private_subnets_cidr_blocks)
  }

  tags = merge(data.aws_vpc.prod.tags, {
    Name = "${local.prefix}-${data.aws_vpc.prod.id}-pl-vpce-sg-rules"
  })
}

//security group to allow access to/from the databricks workers for the kafka interface endpoints
resource "aws_security_group" "broker_interface_endpoints"  {
  name        = "Access to broker interface endpoints"
  description = "security group to allow access to/from the databricks workers for the kafka interface endpoints"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "Inbound rules"
    from_port   = 9096
    to_port     = 9096
    protocol    = "tcp"
    cidr_blocks = [module.vpc.private_subnets_cidr_blocks[0],module.vpc.private_subnets_cidr_blocks[1],module.vpc.private_subnets_cidr_blocks[2]]
  }

  egress {
    description = "Outbound rules"
    from_port   = 9096
    to_port     = 9096
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


resource "aws_vpc_endpoint" "backend_rest" {
  vpc_id             = module.vpc.vpc_id
  service_name       = var.workspace_vpce_service
  vpc_endpoint_type  = "Interface"
  security_group_ids = [aws_security_group.dataplane_vpce.id]
  subnet_ids         = [aws_subnet.dataplane_vpce.id]
  private_dns_enabled = true
  depends_on = [aws_subnet.dataplane_vpce]
}

resource "aws_vpc_endpoint" "relay" {
  vpc_id             = module.vpc.vpc_id
  service_name       = var.relay_vpce_service
  vpc_endpoint_type  = "Interface"
  security_group_ids = [aws_security_group.dataplane_vpce.id]
  subnet_ids         = [aws_subnet.dataplane_vpce.id]
  private_dns_enabled = true
  depends_on = [aws_subnet.dataplane_vpce]
}



resource "databricks_mws_vpc_endpoint" "backend_rest_vpce" {
  provider            = databricks.mws
  account_id          = var.databricks_account_id
  aws_vpc_endpoint_id = aws_vpc_endpoint.backend_rest.id
  vpc_endpoint_name   = "${local.prefix}-vpc-backend-${module.vpc.vpc_id}"
  region              = var.region
  depends_on          = [aws_vpc_endpoint.backend_rest]
}

resource "databricks_mws_vpc_endpoint" "relay" {
  provider            = databricks.mws
  account_id          = var.databricks_account_id
  aws_vpc_endpoint_id = aws_vpc_endpoint.relay.id
  vpc_endpoint_name   = "${local.prefix}-vpc-relay-${module.vpc.vpc_id}"
  region              = var.region
  depends_on          = [aws_vpc_endpoint.relay]
}

resource "databricks_mws_networks" "this" {
  provider           = databricks.mws
  account_id         = var.databricks_account_id
  network_name       = "${local.prefix}-network"
  security_group_ids = [module.vpc.default_security_group_id]
  subnet_ids         = module.vpc.private_subnets
  vpc_id             = module.vpc.vpc_id
  vpc_endpoints {
    dataplane_relay = [databricks_mws_vpc_endpoint.relay.vpc_endpoint_id]
    rest_api        = [databricks_mws_vpc_endpoint.backend_rest_vpce.vpc_endpoint_id]
  }
}

resource "aws_vpc_endpoint" "b1-endpoint" {
  vpc_id            = module.vpc.vpc_id
  service_name      = "com.amazonaws.vpce.us-east-1.vpce-svc-018b515d0ee164c8e"
  vpc_endpoint_type = "Interface"
  subnet_ids =[module.vpc.private_subnets[0]]
  security_group_ids = [
    aws_security_group.broker_interface_endpoints.id,
  ]

  //private_dns_enabled = true
}
// subnet?
resource "aws_vpc_endpoint" "b2-endpoint" {
  vpc_id            = module.vpc.vpc_id
  service_name      = "com.amazonaws.vpce.us-east-1.vpce-svc-0d1b6f7efa6cd9e86"
  vpc_endpoint_type = "Interface"
  security_group_ids = [
    aws_security_group.broker_interface_endpoints.id,
  ]
  subnet_ids = [module.vpc.private_subnets[1]]

}

resource "aws_vpc_endpoint" "b3-endpoint" {
  vpc_id            = module.vpc.vpc_id
  service_name      = "com.amazonaws.vpce.us-east-1.vpce-svc-0c4d33a940b1a009c"
  vpc_endpoint_type = "Interface"
  subnet_ids = [module.vpc.private_subnets[2]]
  security_group_ids = [
    aws_security_group.broker_interface_endpoints.id,
  ]

}

resource "aws_route53_zone" "kafka" {
  name = "kafka.us-east-1.amazonaws.com"
  vpc {
    vpc_id = module.vpc.vpc_id
  }
}

resource "aws_route53_record" "b1_alias" {
  zone_id = aws_route53_zone.kafka.zone_id
  name    = "b-1.rtms-msk.v0fpv1.c2"
  type    = "A"

  alias {
    name                   = aws_vpc_endpoint.b1-endpoint.dns_entry[0].dns_name
    zone_id                = aws_vpc_endpoint.b1-endpoint.dns_entry[0].hosted_zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "b2_alias" {
  zone_id = aws_route53_zone.kafka.zone_id
  name    = "b-2.rtms-msk.v0fpv1.c2"
  type    = "A"

  alias {
    name                   = aws_vpc_endpoint.b2-endpoint.dns_entry[0].dns_name
    zone_id                = aws_vpc_endpoint.b2-endpoint.dns_entry[0].hosted_zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "b3_alias" {
  zone_id = aws_route53_zone.kafka.zone_id
  name    = "b-3.rtms-msk.v0fpv1.c2"
  type    = "A"

  alias {
    name                   = aws_vpc_endpoint.b3-endpoint.dns_entry[0].dns_name
    zone_id                = aws_vpc_endpoint.b3-endpoint.dns_entry[0].hosted_zone_id
    evaluate_target_health = true
  }
}
